package main

import (
	"gitee.com/fierce_wolf/go-fox-edge-iot-common/iotCommInitialize"
	"time"
)

func main() {
	iotCommInitialize.Initialize()

	// 禁止退出
	for true {
		time.Sleep(1000 * time.Second)
	}
}
