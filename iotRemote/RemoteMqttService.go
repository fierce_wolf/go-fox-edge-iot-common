package iotRemote

import (
	"gitee.com/fierce_wolf/go-fox-edge-common/commMqtt"
	"gitee.com/fierce_wolf/go-fox-edge-common/commUtil/periodTask"
	"gitee.com/fierce_wolf/go-fox-edge-iot-common/iotAPI"
)

type remoteMqttService struct {
}

func (e *remoteMqttService) setMqttConfig(mqttParam map[string]interface{}) {
	commMqtt.SetOption(mqttParam, nil)

	// 向MQTT BROKER注册订阅topic
	commMqtt.Subscribe(iotAPI.MqttHandler.GetTopicFunc(), iotAPI.MqttHandler.MqttHandlerFunc())

	// 添加任务
	periodTask.InsertTask(commMqtt.AutoTask)
}

func (e *remoteMqttService) Publish(topic string, payload interface{}) error {
	return commMqtt.Publish(topic, payload)
}
