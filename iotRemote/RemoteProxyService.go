package iotRemote

import (
	"gitee.com/fierce_wolf/go-fox-edge-common/commConfig"
	"gitee.com/fierce_wolf/go-fox-edge-common/commUtil/Map"
)

type remoteProxyService struct {
	mode string
}

func (e *remoteProxyService) Initialize() error {
	// 读取配置参数
	configs, err := commConfig.GetConfigParam("serverConfig")
	if err != nil {
		return err
	}

	remote := Map.GetMap(configs, "remote", make(map[string]interface{}))
	e.mode = Map.GetString(remote, "mode", "http")

	if "http" == (e.mode) {
		httpParam := Map.GetMap(remote, "http", make(map[string]interface{}))
		host := Map.GetString(httpParam, "host", "http://localhost")
		Http.uri = host
	}
	if "mqtt" == (e.mode) {
		mqttParam := Map.GetMap(remote, "mqtt", make(map[string]interface{}))
		Mqtt.setMqttConfig(mqttParam)
	}

	return nil
}
