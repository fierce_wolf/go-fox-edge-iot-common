package iotRemote

import "gitee.com/fierce_wolf/go-fox-edge-common/commUtil/HttpClient"

type remoteHttpService struct {
	header map[string]string
	uri    string
}

func (e *remoteHttpService) ExecuteRestful(res, method, requestJson string) (string, error) {
	if len(e.header) == 0 {
		e.header["Content-Type"] = "application/json"
	}

	return HttpClient.ExecuteRestful(e.uri+res, method, e.header, requestJson)
}
