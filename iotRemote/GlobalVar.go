package iotRemote

var (
	remote *remoteProxyService
	Mqtt   *remoteMqttService
	Http   *remoteHttpService
)

func init() {
	remote = &remoteProxyService{}
	Mqtt = &remoteMqttService{}
	Http = &remoteHttpService{}
}

func Initialize() error {
	return remote.Initialize()
}
