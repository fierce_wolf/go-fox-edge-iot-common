# go-fox-edge-iot-common

#### 组件库的介绍
**Fox-Edge**的GO版本组件库

#### go编译器的安装
ubuntu20自带的版本，比较旧，go的编译器版本，至少使用1.18以上版本，所以下载go官网的版本
``` 
wget https://dl.google.com/go/go1.18.linux-arm64.tar.gz

tar -C /usr/local -xzf go1.18.linux-arm64.tar.gz

export PATH=$PATH:/usr/local/go/bin

#对应的远程调试工具
go install github.com/go-delve/delve/cmd/dlv@v1.8.3
``` 

#### 组件库的初始化

``` 
#go mod的初始化
#gitee仓库：https://gitee.com/fierce_wolf/fox-edge-kernel-common-go.git
go mod init gitee.com/fierce_wolf/fox-edge-kernel-common-go
``` 

#### 组件库的提交
在git上提交代码后，go需要在git仓库打上版本号(v1.x.x格式)的tag，那么后面其他go工程就可以使用这个库了

``` 
# 在git提交代码后，再在git库上打上go需要的tag 版本标识
git tag v1.0.0
git push origin v1.0.0
```

#### 组件库的引用
go默认的GOPROXY是https://proxy.golang.org非常的缓慢，可以切换为国内的镜像goproxy.cn来解决

``` 
#go默认的GOPROXY是https://proxy.golang.org非常的缓慢，可以切换为国内的镜像goproxy.cn来解决
go env -w GOPROXY=https://goproxy.cn,direct

#查询最新可用版本
go list -m -versions fox-edge-channel-common-go

#可以使用该命令下载gitee上的仓库到本地计算机上
go list

#下载云端的git库和第三方的git库
go mod tidy

#可以本地测试自己的go工程代码
go run main.go

#也可以windows本地编译
go build -o main.exe
```





#### IDEA的命令行参数

redis.host=192.168.1.23 --env_dev=true --app_engine=native --app_type=service --app_name=iot-fox-publish-native --env_type=device --env_cpu_id= --env_database=sqlite3 --work_mode=local server.port=9101 redis.port=6379 redis.password=12345678 mysql.host=192.168.1.23 mysql.port=3306 mysql.username=fox-edge mysql.password=12345678 mysql.database=fox_edge param1=1234 param2=567

