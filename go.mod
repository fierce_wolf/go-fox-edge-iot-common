module gitee.com/fierce_wolf/go-fox-edge-iot-common

go 1.18

require (
	gitee.com/fierce_wolf/go-fox-edge-common v1.0.0
	github.com/go-sql-driver/mysql v1.8.1
	github.com/mattn/go-sqlite3 v1.14.24
)

require (
	filippo.io/edwards25519 v1.1.0 // indirect
	github.com/eclipse/paho.mqtt.golang v1.5.0 // indirect
	github.com/go-redis/redis v6.15.9+incompatible // indirect
	github.com/gorilla/websocket v1.5.3 // indirect
	github.com/nxadm/tail v1.4.11 // indirect
	github.com/satori/go.uuid v1.2.0 // indirect
	golang.org/x/net v0.27.0 // indirect
	golang.org/x/sync v0.8.0 // indirect
	golang.org/x/text v0.19.0 // indirect
	golang.org/x/xerrors v0.0.0-20240903120638-7835f813f4da // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
)

//replace gitee.com/fierce_wolf/go-fox-edge-common => ..\..\go-fox-edge-common // 替换成本地 a项目的路径  （这里用的绝对路径）
