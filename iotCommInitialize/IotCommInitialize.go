package iotCommInitialize

import (
	"gitee.com/fierce_wolf/go-fox-edge-common/commEntityManager"
	"gitee.com/fierce_wolf/go-fox-edge-common/commEnv"
	"gitee.com/fierce_wolf/go-fox-edge-common/commLogger"
	"gitee.com/fierce_wolf/go-fox-edge-common/commRedis"
	"gitee.com/fierce_wolf/go-fox-edge-common/commRedisStatus"
	"gitee.com/fierce_wolf/go-fox-edge-common/commUtil/periodTask"
	"gitee.com/fierce_wolf/go-fox-edge-iot-common/iotAPI"
	"gitee.com/fierce_wolf/go-fox-edge-iot-common/iotRemote"
)

func Initialize() {
	commEnv.SetWorkPath(2)

	// 连接redis
	commRedis.ConnectRedis()

	// 检查连接状态：连接不上就通过panic退出吧
	ok := commRedis.TestConnect()
	if !ok {
		panic("连接redis失败:" + commRedis.Options.Addr)
	}

	commLogger.Info("------------- iot-common-native 初始化开始！-------------")

	// 启动状态线程
	commRedisStatus.Instance()
	commRedisStatus.Scheduler()

	// 绑定已经实现的接口
	iotAPI.EntityManager.InstanceFunc()
	iotAPI.EntityManager.InitLoadEntityFunc()

	// 将全局配置，读取到本地缓存中，方便后面反复使用，该方法必须在this.entityManageService.initLoadEntity()之后执行
	iotAPI.LocalConfig.InitializeFunc()

	// 远程通信组件的初始化
	iotRemote.Initialize()

	// 启动同步线程
	commEntityManager.Scheduler()

	// 启动周期线程
	periodTask.Scheduler()

	commLogger.Info("------------- iot-common-native 初始化结束！-------------")

}
