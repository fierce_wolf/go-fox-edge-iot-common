package iotAPI

import mqtt "github.com/eclipse/paho.mqtt.golang"

type iotEntityManager struct {
	Instance       IEntityManagerInstance       // Instance：指明要装载的方式
	InitLoadEntity IEntityManagerInitLoadEntity // InitLoadEntity：指明如何转载数据
}

func (e *iotEntityManager) InstanceFunc() {
	e.Instance.Instance()
}

func (e *iotEntityManager) InitLoadEntityFunc() {
	e.InitLoadEntity.InitLoadEntity()
}

type iotLocalConfig struct {
	Initialize IConfig // Instance：指明要装载的方式
}

func (e *iotLocalConfig) InitializeFunc() {
	e.Initialize.Initialize()
}

type iotMqttHandler struct {
	MqttHandler IMqttHandler // MqttHandler：指明要装载Handler
}

func (e *iotMqttHandler) GetTopicFunc() string {
	return e.MqttHandler.GetTopic()
}

func (e *iotMqttHandler) MqttHandlerFunc() mqtt.MessageHandler {
	return e.MqttHandler.MqttHandler()
}
