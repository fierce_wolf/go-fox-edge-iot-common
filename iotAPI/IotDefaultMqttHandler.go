package iotAPI

import (
	"fmt"
	mqtt "github.com/eclipse/paho.mqtt.golang"
)

type iotDefaultMqttHandler struct {
}

func (e *iotDefaultMqttHandler) GetTopic() string {
	return "#"
}

func (e *iotDefaultMqttHandler) MqttHandler() mqtt.MessageHandler {
	return func(client mqtt.Client, msg mqtt.Message) {
		fmt.Printf("TOPIC: %s\n", msg.Topic())
		fmt.Printf("MSG: %s\n", msg.Payload())
	}
}
