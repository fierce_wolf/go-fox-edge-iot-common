package iotAPI

import mqtt "github.com/eclipse/paho.mqtt.golang"

type IEntityManagerInstance interface {
	Instance()
}

type IEntityManagerInitLoadEntity interface {
	InitLoadEntity()
}

type IConfig interface {
	Initialize()
}

type IMqttHandler interface {
	GetTopic() string
	MqttHandler() mqtt.MessageHandler
}
