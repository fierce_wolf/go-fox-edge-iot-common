package iotAPI

var (
	EntityManager *iotEntityManager
	LocalConfig   *iotLocalConfig
	MqttHandler   *iotMqttHandler
)

func init() {
	EntityManager = &iotEntityManager{}
	defaultManager := &iotDefaultEntityManager{}
	EntityManager.Instance = defaultManager
	EntityManager.InitLoadEntity = defaultManager

	LocalConfig = &iotLocalConfig{}
	LocalConfig.Initialize = &iotDefaultLocalConfig{}

	MqttHandler = &iotMqttHandler{}
	MqttHandler.MqttHandler = &iotDefaultMqttHandler{}
}
